import org.junit.Assert;
import org.junit.Test;

public class OlaMundoTest {
    @Test
    public void devoDizerOlaQuandoNomeForInformado(){
        OlaMundo olaMundo = new OlaMundo();
        String resposta = olaMundo.dizerOla("Victor");

        Assert.assertEquals("Olá Victor!", resposta);
    }
}
